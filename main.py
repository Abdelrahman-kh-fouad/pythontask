import sqlite3
from datetime import datetime
from zoneinfo import ZoneInfo
import json
db_connection = sqlite3.connect("attendance.db")
db_cur = db_connection.cursor()
db_cur.execute("PRAGMA foreign_keys = ON")

def emp_actions(emp: str):
    return [x for x in db_cur.execute('''
            SELECT ActionTime, Action, day, employee
            FROM AttendanceActions 
            INNER JOIN Attendance
            ON AttendanceActions.AttendanceId = Attendance.Id
            WHERE Attendance.employee = "{0}";
    '''.format(emp.upper()))]

def date(x: str):
    res = []
    dates = x.split(' ')
    res.extend([int(x) for x in dates[0].split('-')][:])
    if len(dates) >1:
        in_time = datetime.strptime(''.join(dates[1:]), "%I:%M%p")
        out_time = datetime.strftime(in_time, "%H:%M")
        res.extend([int(x) for x in out_time.split(':')])
    else:
        res.extend(0,0)
    return datetime(*res, tzinfo=ZoneInfo("Egypt"))


def get_attendance(emp: str, day: str):
    res = {"attended": False}
    emp_actions_list = emp_actions(emp)
    start = None
    end = None
    for i in emp_actions_list:
        if i[1] == 'CheckIn' and start == None and i[2] == day:
            start = date(i[0])
        if start != None and i[1] == 'CheckOut' and end == None:
            if date(i[0]) >= start:
                end = date(i[0])
    if start != None and  end != None:
        res['attended'] = True
        res['duration'] = ':'.join(str(end-start).split(':')[0:2])
    return json.dumps(res, indent=2)

def get_all(emp: str):
    res= {}
    emp_actions_list = emp_actions(emp)
    for i in emp_actions_list:
        if i[2] not in res:
            res[i[2]] =[]
        res[i[2]].append({"action": i[1], "time": str(date(i[0]).astimezone(ZoneInfo('UTC')).isoformat())})
    n_res =[]
    for x, y in res.items():
        n_res.append({"date": x, "actions": y})
    return json.dumps(n_res, indent=2)

if __name__ == '__main__':
    print(get_attendance('emp01', '2020-04-01'))
    print(get_all('emp01'))
